import Home from "../components/OverView";
import React from "react";
import Login from "../components/Login";
import ServerConfig from "../components/serverConfig";
import AddOverView from "../components/Driver/OverView";
import DriverList from "../components/Driver/DriverList";
import MobileConfig from "../components/MobileConig";
import Layout from "../components/Layout/layout";
import AddDriver from "../components/Driver/AddDriver";
import CustomerList from "../components/Rider/CustomerList";
import DriverProfile from "../components/Driver/DriverList/DriverProfile.js";
import AddCustomer from "../components/Rider/AddCustomer";
import Rider from "../components/Rider/OverView";
import CustomerProfile from "../components/Rider/CustomerList/CustomerProfile";
import {BrowserRouter as Router, Route, Redirect} from "react-router-dom";
const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={Login} />
      <Route
        exact
        path="/server-configuration"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <ServerConfig />
          )
        }
      />
      <Route
        exact
        path="/home"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <Home />
          )
        }
      />
      <Route
        exact
        path="/add-customer"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <AddCustomer />
          )
        }
      />
      <Route
        exact
        path="/add-driver"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <AddDriver />
          )
        }
      />
      <Route exact path="/driver-profile/:Id" component={DriverProfile} />

      <Route
        exact
        path="/driver"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <AddOverView />
          )
        }
      />
      <Route
        exact
        path="/driver-list"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <DriverList />
          )
        }
      />
      <Route
        exact
        path="/customer"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <Rider />
          )
        }
      />
      <Route
        exact
        path="/customer-profile/:Id"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <CustomerProfile />
          )
        }
      />
      <Route
        exact
        path="/customer-list"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <CustomerList />
          )
        }
      />
      <Route
        exact
        path="/mobile-app-configuration"
        render={() =>
          !localStorage.getItem("id_token") ? (
            <Redirect from="/home" to="/" />
          ) : (
            <MobileConfig />
          )
        }
      />
    </div>
  </Router>
);
export default Routes;
