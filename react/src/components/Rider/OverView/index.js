import React, {Component} from "react";
import Layout from "../../Layout/layout";
import Overview from "./overview";

export default class RiderOverview extends Component {
  render() {
    return (
      <Layout>
        <Overview />
      </Layout>
    );
  }
}
