import React, {Component} from "react";
import Layout from "../../Layout/layout";
import AddCustomer from "./addCustomer";

export default class Overview extends Component {
  render() {
    return (
      <Layout>
        <AddCustomer />
      </Layout>
    );
  }
}
