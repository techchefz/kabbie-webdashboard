import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Panel,
  Button,
  Col,
  Row
} from "react-bootstrap";
import InputGroup, {
  Addon as InputGroupAddon
} from "react-bootstrap/lib/InputGroup";
import TripAction from "../../redux/trips/action";

class CustomerManagement extends Component {
  static propTypes = {
    requestTripLoading: PropTypes.bool,
    errorStatusCreateTripObj: PropTypes.bool,
    newTripObj: PropTypes.object,
    errorTripObj: PropTypes.object,
    createNewTrip: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      tripObj: {
        riderId: null,
        driverId: null,
        pickUpAddress: "NA",
        destAddress: "NA",
        paymentMode: "NA",
        taxiType: "NA"
      }
    };
    this.handleRiderId = this.handleRiderId.bind(this);
    this.handleDriverId = this.handleDriverId.bind(this);
    this.handlePickUpAddress = this.handlePickUpAddress.bind(this);
    this.handleDestAddress = this.handleDestAddress.bind(this);
    this.handlePaymentMode = this.handlePaymentMode.bind(this);
    this.handleTaxiType = this.handleTaxiType.bind(this);
  }
  handleRiderId(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.riderId = e.target.value;
    this.setState({tripObj});
  }
  handleDriverId(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.driverId = e.target.value;
    this.setState({tripObj});
  }
  handlePickUpAddress(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.pickUpAddress = e.target.value;
    this.setState({tripObj});
  }
  handleDestAddress(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.destAddress = e.target.value;
    this.setState({tripObj});
  }
  handlePaymentMode(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.paymentMode = e.target.value;
    this.setState({tripObj});
  }
  handleTaxiType(e) {
    e.preventDefault();
    const tripObj = this.state.tripObj;
    tripObj.taxiType = e.target.value;
    this.setState({tripObj});
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.createNewTrip(this.state.tripObj);
  }
  render() {
    return (
      <Row className="animate">
        <Col md={12}>
          <Panel header="Create New Trip" bsStyle="primary">
            {this.props.errorStatusCreateTripObj ? (
              <div className="alert alert-danger">
                <strong>{this.props.errorTripObj.message}</strong>
              </div>
            ) : null}
            {this.props.requestTripLoading ? (
              <div
                id="tripObjectLoading"
                className="loading-wrap"
                style={{minHeight: 500}}
              >
                <div className="loading">
                  <div id="spinner">
                    <svg
                      className="spinner"
                      width="65px"
                      height="65px"
                      viewBox="0 0 66 66"
                    >
                      <circle
                        className="path"
                        fill="none"
                        strokeWidth="4"
                        strokeLinecap="round"
                        cx="33"
                        cy="33"
                        r="30"
                      />
                    </svg>
                  </div>
                </div>
              </div>
            ) : (
              <Form
                horizontal
                onSubmit={e => {
                  this.handleSubmit(e);
                }}
              >
                <Row>
                  <div className="col-md-5">
                    <FormGroup>
                      <ControlLabel>Rider Id</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-user" />
                        </InputGroupAddon>
                        <FormControl
                          type="text"
                          placeholder="Rider Id"
                          onChange={e => {
                            this.handleRiderId(e);
                          }}
                        />
                      </InputGroup>
                    </FormGroup>
                  </div>
                  <div className="col-md-5 col-md-offset-1">
                    <FormGroup>
                      <ControlLabel>Driver Id</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-user" />
                        </InputGroupAddon>
                        <FormControl
                          type="text"
                          placeholder="Driver Id"
                          onChange={e => {
                            this.handleDriverId(e);
                          }}
                        />
                      </InputGroup>
                    </FormGroup>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-5">
                    <FormGroup>
                      <ControlLabel>Pickup Address</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-map-marker" />
                        </InputGroupAddon>
                        <FormControl
                          componentClass="textarea"
                          rows={3}
                          maxLength={4000}
                          placeholder="Source Addrress"
                          onChange={e => {
                            this.handlePickUpAddress(e);
                          }}
                        />
                      </InputGroup>
                    </FormGroup>
                  </div>
                  <div className="col-md-5 col-md-offset-1">
                    <FormGroup>
                      <ControlLabel>Destination Address</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-map-marker" />
                        </InputGroupAddon>
                        <FormControl
                          componentClass="textarea"
                          rows={3}
                          maxLength={4000}
                          placeholder="Destination Addrress"
                          onChange={e => {
                            this.handleDestAddress(e);
                          }}
                        />
                      </InputGroup>
                    </FormGroup>
                  </div>
                </Row>
                <Row>
                  <div className="col-md-5">
                    <FormGroup>
                      <ControlLabel>Payment Mode</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-credit-card" />
                        </InputGroupAddon>
                        <FormControl
                          componentClass="select"
                          placeholder="Payment Mode"
                          onChange={e => {
                            this.handlePaymentMode(e);
                          }}
                        >
                          <option value="select">Select</option>
                          <option value="cash">Cash</option>
                          <option value="paytm">Paytm</option>
                        </FormControl>
                      </InputGroup>
                    </FormGroup>
                  </div>
                  <div className="col-md-5 col-md-offset-1">
                    <FormGroup>
                      <ControlLabel>Taxi Type</ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-bed" />
                        </InputGroupAddon>
                        <FormControl
                          componentClass="select"
                          placeholder="Taxi Type"
                          onChange={e => {
                            this.handleTaxiType(e);
                          }}
                        >
                          <option value="select">Select</option>
                          <option value="taxiGo">Taxi Go</option>
                          <option value="taxiMini">Taxi Mini</option>
                          <option value="taxiPrime">Taxi Prime</option>
                          <option value="taxiMacro">Taxi Macro</option>
                        </FormControl>
                      </InputGroup>
                    </FormGroup>
                  </div>
                </Row>
                <Button type="reset"> Reset </Button> &nbsp;&nbsp;
                <Button type="submit" bsStyle="primary">
                  {" "}
                  Submit{" "}
                </Button>
              </Form>
            )}
          </Panel>
        </Col>
      </Row>
    );
  }
}

function mapStateToProp(state) {
  return {
    requestTripLoading: state.trips.requestTripLoading,
    errorStatusCreateTripObj: state.trips.errorStatusCreateTripObj,
    errorTripObj: state.trips.errorTripObj,
    newTripObj: state.trips.newTripObj
  };
}

function bindActions(dispatch) {
  return {
    createNewTrip: tripObj => dispatch(TripAction.createNewTrip(tripObj))
  };
}

export default connect(mapStateToProp, bindActions)(CustomerManagement);
