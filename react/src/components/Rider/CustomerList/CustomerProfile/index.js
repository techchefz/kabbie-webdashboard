import React, {Component} from "react";
import Layout from "../../../Layout/layout";
import Profile from "./customerProfile";

export default class customerProfile extends Component {
  render() {
    return (
      <Layout>
        <Profile />
      </Layout>
    );
  }
}
