import React, {Component} from "react";
import Layout from "../Layout/layout";
import Management from "./CustomerManagement";

export default class Overview extends Component {
  render() {
    return (
      <Layout>
        <Management />
      </Layout>
    );
  }
}
