import React, { Component } from 'react';
import _ from "lodash";
import moment from "moment";
import { FormattedMessage } from "react-intl";
import RecentReviewRating from "../UserRatingComponent";
import "../../styles/common/DriverList.scss"

export default class RideDetails extends Component {
    render() {
        return (
            <div>
                <div>
                    <table className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <thead>
                            <tr className="panelTableHead" style={{ backgroundColor: "#bdbdbd", color: "white", border: "5px white solid" }}>
                                <td>
                                    <strong style={{ marginLeft: "45%" }}> <FormattedMessage
                                        id={"riderDetails"}
                                        defaultMessage={"RIDER DETAILS"}
                                    /> </strong>
                                </td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody className="panelTableTBody">
                            <tr>
                                <td>Rider Name </td>
                                <td>{_.get(this.props.rideObj.riderId, "fname", "NA")} {_.get(this.props.rideObj.riderId, "lname", "NA")}</td>
                            </tr>
                            <tr>
                                <td>Contact</td>
                                <td>{_.get(this.props.rideObj.riderId, "phoneNo", "NA")}</td>
                            </tr>
                            <tr>
                                <td>EMAIL </td>
                                <td>{_.get(this.props.rideObj.riderId, "email", "NA")}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <thead>
                            <tr className="panelTableHead" style={{ backgroundColor: "#bdbdbd", color: "white", border: "5px white solid" }}>
                                <td>
                                    <strong style={{ marginLeft: "45%" }}> <FormattedMessage
                                        id={"driverDetails"}
                                        defaultMessage={"DRIVER DETAILS"}
                                    /> </strong>
                                </td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody className="panelTableTBody">
                            <tr>
                                <td>Driver Name </td>
                                <td>{_.get(this.props.rideObj.driverId, "fname", "NA")} {_.get(this.props.rideObj.driverId, "lname", "NA")}</td>
                            </tr>
                            <tr>
                                <td>Contact</td>
                                <td>{_.get(this.props.rideObj.driverId, "phoneNo", "NA")}</td>
                            </tr>
                            <tr>
                                <td>EMAIL </td>
                                <td>{_.get(this.props.rideObj.driverId, "email", "NA")}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <thead>
                            <tr className="panelTableHead" style={{ backgroundColor: "#bdbdbd", color: "white", border: "5px white solid" }}>
                                <td>
                                    <strong style={{ marginLeft: "45%" }}>  <FormattedMessage

                                        id={"rideHistoryDetails"}
                                        defaultMessage={"RIDE DETAILS"}
                                    /> </strong>
                                </td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody className="panelTableTBody">
                            <tr>
                                <td>RIDE ID</td>
                                <td><strong>{_.get(this.props.rideObj, "_id", "NA")}</strong></td>
                            </tr>
                            <tr>
                                <td>BOOKING TIME</td>
                                <td>{moment(_.get(this.props.rideObj, "bookingTime", "NA")).format("DD MMM YYYY hh:mm a")}</td>
                            </tr>
                            <tr>
                                <td>SOURCE</td>
                                <td>{_.get(this.props.rideObj, "pickUpAddress", "NA")}</td>
                            </tr>
                            <tr>
                                <td>DESTINATION</td>
                                <td>{_.get(this.props.rideObj, "destAddress", "NA")}</td>
                            </tr>
                            <tr>
                                <td>TAXI TYPE</td>
                                <td>{_.get(this.props.rideObj, "taxiType", "NA")}</td>
                            </tr>
                            <tr>
                                <td>PAYMENT MODE</td>
                                <td>{_.get(this.props.rideObj, "paymentMode", "NA")}</td>
                            </tr>
                            <tr>
                                <td>AMOUNT</td>
                                <td>{_.get(this.props.rideObj, "tripAmt", "NA")}</td>
                            </tr>
                            <tr>
                                <td>RATING</td>
                                <td>
                                    <RecentReviewRating
                                        noofstars={5}
                                        ratedStar={_.get(this.props.rideObj, "driverRatingByRider", "0")}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
}
