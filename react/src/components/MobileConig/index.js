import React, {Component} from "react";
import Layout from "../Layout/layout";
import MobileConfiguration from "./MobileConfiguration";

export default class OverViews extends Component {
  render() {
    return (
      <Layout>
        <MobileConfiguration />
      </Layout>
    );
  }
}
