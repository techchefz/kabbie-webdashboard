import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl, ButtonToolbar, ToggleButtonGroup, ToggleButton } from "react-bootstrap";
import PropTypes from "prop-types";
import _ from "lodash";
import { FormattedMessage } from "react-intl";
import "../../styles/common/mobileapp_configuration.scss";

class tripPriceCalculation extends Component {

  state = {
    rideType: 'Quickest',
    currentType: {
      currencySymbol: "$",
      baseFare: 0,
      farePerKm: 0,
      farePerMin: 0
    },
    rideObj: null,
    show: null
  }

  static propTypes = {
    tripObj: PropTypes.object,
    onUpdateTrip: PropTypes.func
  };

  componentWillMount = () => {
    this.setState({ rideObj: this.props.tripObj, currentType: this.props.tripObj.quickestPrice });
  }


  handleChange(value, label, updateReq) {
    if (this.state.rideType === 'Quickest') {
      if (label !== "currencySymbol") {
        this.props.tripObj.quickestPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.quickestPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.quickestPrice, "quickestPrice", updateReq);

    } else if (this.state.rideType === 'Sedan') {
      if (label !== "currencySymbol") {
        this.props.tripObj.sedanPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.sedanPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.sedanPrice, "sedanPrice", updateReq);

    } else if (this.state.rideType === 'Luxury') {
      if (label !== "currencySymbol") {
        this.props.tripObj.luxuryPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.luxuryPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.luxuryPrice, "luxuryPrice", updateReq);

    } else if (this.state.rideType === 'Wagon') {
      if (label !== "currencySymbol") {
        this.props.tripObj.wagonPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.wagonPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.wagonPrice, "wagonPrice", updateReq);

    } else if (this.state.rideType === 'WheelchairOne') {
      if (label !== "currencySymbol") {
        this.props.tripObj.wheelchairOnePrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.wheelchairOnePrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.wheelchairOnePrice, "wheelchairOnePrice", updateReq);

    } else if (this.state.rideType === 'WheelchairTwo') {
      if (label !== "currencySymbol") {
        this.props.tripObj.wheelchairTwoPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.wheelchairTwoPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.wheelchairTwoPrice, "wheelchairTwoPrice", updateReq);

    }
    else if (this.state.rideType === 'MaxiFive') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiFivePrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiFivePrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiFivePrice, "maxiFivePrice", updateReq);
    }
    else if (this.state.rideType === 'MaxiSix') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiSixPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiSixPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiSixPrice, "maxiSixPrice", updateReq);
    }
    else if (this.state.rideType === 'MaxiSeven') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiSevenPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiSevenPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiSevenPrice, "maxiSevenPrice", updateReq);
    }
    else if (this.state.rideType === 'MaxiEight') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiEightPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiEightPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiEightPrice, "maxiEightPrice", updateReq);
    }
    else if (this.state.rideType === 'MaxiNine') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiNinePrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiNinePrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiNinePrice, "maxiNinePrice", updateReq);
    } else if (this.state.rideType === 'MaxiTen') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiTenPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiTenPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiTenPrice, "maxiTenPrice", updateReq);
    } else if (this.state.rideType === 'MaxiEleven') {
      if (label !== "currencySymbol") {
        this.props.tripObj.maxiElevenPrice[label] = parseFloat(value);
      } else {
        this.props.tripObj.maxiElevenPrice[label] = value;
      }
      this.props.onUpdateTrip(this.props.tripObj.maxiElevenPrice, "maxiElevenPrice", updateReq);
    }

  }

  clearFormData() {

  }

  render() {
    return (
      <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div className="panel panel-primary Panelprice">
          <div className="panel-heading">
            <span>
              {" "}
              <FormattedMessage
                id={"trip_price"}
                defaultMessage={"Trip Price Calculation"}
              />
            </span>
          </div>
          <div className="panel-body panelBody" style={{ padding: 0 }}>
            <div className="priceCalculation">
              <div className={this.state.rideType === 'Quickest' ? "carTypeActive" : "carType"}
                onClick={() =>
                  this.setState({ currentType: this.props.tripObj.quickestPrice, rideType: 'Quickest', show: null })
                }>Quickest
                </div>
              <div className={this.state.rideType === 'Sedan' ? "carTypeActive" : "carType"}
                onClick={() =>
                  this.setState({ currentType: this.props.tripObj.sedanPrice, rideType: 'Sedan', show: null })
                }>
                Sedan
                </div>
              <div className={this.state.rideType === 'Luxury' ? "carTypeActive" : "carType"}
                onClick={() =>
                  this.setState({ currentType: this.props.tripObj.luxuryPrice, rideType: 'Luxury', show: null })
                }>
                Luxury
                </div>
              <div className={this.state.rideType === 'Wagon' ? "carTypeActive" : "carType"}
                onClick={() =>
                  this.setState({ currentType: this.props.tripObj.wagonPrice, rideType: 'Wagon', show: null })
                }>
                Wagon
                </div>
              <div className={this.state.rideType === 'WheelchairOne' ? "carTypeActive" : "carType"}
                onClick={() => this.setState({ currentType: this.props.tripObj.wheelchairOnePrice, rideType: 'WheelchairOne', show: 'Wheelchair' })}>
                Wheelchair
                </div>
              <div className={this.state.rideType === 'MaxiFive' ? "carTypeActive" : "carType"}
                onClick={() => this.setState({ currentType: this.props.tripObj.maxiFivePrice, rideType: 'MaxiFive', show: 'Maxi' })}>
                Maxi
                </div>

            </div>
            {console.log(this.state.rideType)}
            {
              this.state.show == "Wheelchair" ?
                <div className="d-flex flex-nowrap justify-content-center">
                  <ButtonToolbar>
                    <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
                      <ToggleButton bsStyle="warning" value={1} onClick={() => this.setState({ currentType: this.props.tripObj.wheelchairOnePrice, rideType: 'WheelchairOne' })}>Wheelchair One</ToggleButton>
                      <ToggleButton bsStyle="warning" value={2} onClick={() => this.setState({ currentType: this.props.tripObj.wheelchairTwoPrice, rideType: 'WheelchairTwo' })}>Wheelchair Two</ToggleButton>
                    </ToggleButtonGroup>
                  </ButtonToolbar>
                </div>
                : null
            }
            {this.state.show == "Maxi" ?
              <div className="d-flex justify-content-center">
                <ButtonToolbar>
                  <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
                    <ToggleButton bsStyle="warning" value={1} onClick={() => this.setState({ currentType: this.props.tripObj.maxiFivePrice, rideType: 'MaxiFive' })}>Maxi 5</ToggleButton>
                    <ToggleButton bsStyle="warning" value={2} onClick={() => this.setState({ currentType: this.props.tripObj.maxiSixPrice, rideType: 'MaxiSix' })}>Maxi 6</ToggleButton>
                    <ToggleButton bsStyle="warning" value={3} onClick={() => this.setState({ currentType: this.props.tripObj.maxiSevenPrice, rideType: 'MaxiSeven' })}>Maxi 7</ToggleButton>
                    <ToggleButton bsStyle="warning" value={4} onClick={() => this.setState({ currentType: this.props.tripObj.maxiEightPrice, rideType: 'MaxiEight' })}>Maxi 8</ToggleButton>
                    <ToggleButton bsStyle="warning" value={5} onClick={() => this.setState({ currentType: this.props.tripObj.maxiNinePrice, rideType: 'MaxiNine' })}>Maxi 9</ToggleButton>
                    <ToggleButton bsStyle="warning" value={6} onClick={() => this.setState({ currentType: this.props.tripObj.maxiTenPrice, rideType: 'MaxiTen' })}>Maxi 10</ToggleButton>
                    <ToggleButton bsStyle="warning" value={7} onClick={() => this.setState({ currentType: this.props.tripObj.maxiElevenPrice, rideType: 'MaxiEleven' })}>Maxi 11</ToggleButton>
                  </ToggleButtonGroup>
                </ButtonToolbar>
              </div> :
              null
            }

            <Form inline className="col-md-12 col-lg-12 col-sm-12 form">

              <FormGroup
                controlId="formInlineName"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4 ">
                  <FormattedMessage
                    id={"currency_symbol"}
                    defaultMessage={"Currency Symbol"}
                  />
                </ControlLabel>

                <FormControl
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  type="text"
                  placeholder={_.get(this.state.currentType, "currencySymbol", "$")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "currencySymbol", true);
                    event.target.value = ""
                  }}
                />
              </FormGroup>
              <FormGroup
                controlId="formInlineEmail"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  {" "}
                  <FormattedMessage
                    id={"base_fare"}
                    defaultMessage={"Base Fare:"}
                  />
                </ControlLabel>
                <FormControl
                  type="email"
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "baseFare", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "baseFare", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>

              <FormGroup
                controlId="formInlineName"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  <FormattedMessage
                    id={"fare_km"}
                    defaultMessage={"Fare Per Km:"}
                  />
                </ControlLabel>

                <FormControl
                  type="text"
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "farePerKm", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "farePerKm", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>

              <FormGroup
                controlId="formInlineEmail"
                className="col-md-12 col-lg-12 col-sm-12 formgroup"
              >
                <ControlLabel className="col-md-4 col-lg-4 col-sm-4">
                  <FormattedMessage
                    id={"fare_min"}
                    defaultMessage={"Fare Per Min:"}
                  />
                </ControlLabel>

                <FormControl
                  className="col-md-8 col-lg-8 col-sm-8 formcontrol"
                  placeholder={_.get(this.state.currentType, "farePerMin", "")}
                  onBlur={event => {
                    this.handleChange(event.target.value, "farePerMin", true);
                    event.target.value = ""
                  }
                  }
                />
              </FormGroup>
            </Form>
          </div>
        </div>
      </div >
    );
  }
}

export default tripPriceCalculation;
