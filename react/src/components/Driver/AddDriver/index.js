import React, { Component } from "react";
import Layout from "../../Layout/layout";
import Driver from "./Driver";

export default class AddDriver extends Component {
  render() {
    return (
      <Layout>
        <Driver />
      </Layout>
    );
  }
}
