import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { FormattedMessage } from "react-intl";
import "../../../styles/common/add_driver.scss";

const validate = values => {
  const errors = {};
  if (!values.RC_ownerName) {
    errors.RC_ownerName = "Required";
  }
  if (!values.regNo) {
    errors.regNo = "Required";
  }
  if (!values.type) {
    errors.type = "Required";
  }
  if (!values.company) {
    errors.company = "Required";
  }
  if (!values.carModel) {
    errors.carModel = "Required";
  }
  return errors;
};

const renderField = ({
  input,
  label,
  placeholder,
  type,
  className,
  OnformBlur,
  meta: { touched, error, warning }
}) => (
  <div>
    {label !== "VN" ? (
      <label className="col-md-4 col-lg-4 col-sm-4 formlabel"> {label}</label>
    ) : (
      <span style={{ width: 0 }} />
    )}
    <div>
      <input
        className={className}
        {...input}
        type={type}
        placeholder={placeholder}
        onBlur={e => {
          input.onBlur(e);
          OnformBlur(input.name, input.value, label);
        }}
      />
      {touched &&
        (error && (
          <span style={{ color: "red", textAlign: "center", display: "block" }}>
            {error}
          </span>
        ))}
    </div>
  </div>
);
class vehicleInfoForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func,
    OnformBlur: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      isfetched: false
    };
  }
  render() {
    return (
      <form className="form">
        <div className="col-md-12 col-lg-12">
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label>Rc Owner Name</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="RC_ownerName"
                  component={renderField}
                  type="text"
                  placeholder="RC Owner Name"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label>Registered RTO</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="regNo"
                  component={renderField}
                  type="text"
                  placeholder="Registered RTO"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label>Manufacturer</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="company"
                  component={renderField}
                  type="text"
                  placeholder="Manufacturer"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label>Vehicle No</label>
                <div>
                  <div className="col-sm-3 col-xs-3">
                    <Field
                      className="col-md-3 col-lg-3 col-sm-3 form-control"
                      name="state"
                      label="VN"
                      component={renderField}
                      placeholder="KA"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-3 col-xs-3">
                    <Field
                      className="col-md-3 col-lg-3 col-sm-3 form-control"
                      name="sno"
                      label="VN"
                      component={renderField}
                      placeholder="55"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-3 col-xs-3">
                    <Field
                      className="col-md-3 col-lg-3 col-sm-3 form-control"
                      name="div"
                      label="VN"
                      component={renderField}
                      placeholder="AS"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-3 col-xs-3">
                    <Field
                      className="col-md-3 col-lg-3 col-sm-3 form-control"
                      name="vehicleno"
                      label="VN"
                      component={renderField}
                      placeholder="1245"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label>Vehicle Model</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="carModel"
                  component={renderField}
                  type="text"
                  placeholder="Vehicle Model"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label>Registration Date</label>
                <div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="month"
                      component={renderField}
                      placeholder="MM"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="date"
                      component={renderField}
                      placeholder="DD"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="year"
                      component={renderField}
                      placeholder="YYYY"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: "vehicledriver", // a unique identifier for this form
  validate // <--- validation function given to redux-form
})(vehicleInfoForm);
