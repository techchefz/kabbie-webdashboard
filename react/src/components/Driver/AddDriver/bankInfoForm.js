import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { FormattedMessage } from "react-intl";
import "../../../styles/common/add_driver.scss";

const validate = values => {
  const errors = {};
  if (!values.accountNo) {
    errors.accountNo = "Required";
  }
  if (!values.holderName) {
    errors.holderName = "Required";
  }
  if (!values.IFSC) {
    errors.IFSC = "Required";
  }
  return errors;
};

const renderField = ({
  input,
  label,
  placeholder,
  type,
  className,
  OnformBlur,
  meta: { touched, error, warning }
}) => (
  <div>
    <label className="col-md-4 col-lg-4 col-sm-4 formlabel"> {label}</label>
    <div>
      <input
        className={className}
        {...input}
        type={type}
        placeholder={placeholder}
        onBlur={e => {
          input.onBlur(e);
          OnformBlur(input.name, input.value);
        }}
      />
      {touched &&
        (error && (
          <span style={{ color: "red", textAlign: "center", display: "block" }}>
            {error}
          </span>
        ))}
    </div>
  </div>
);

class bankInfoForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func,
    OnformBlur: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      isfetched: false
    };
  }
  render() {
    return (
      <form className="form">
        <div className="col-md-12 col-lg-12">
          <div className="form-group">
            <label>Account No.</label>
            <Field
              className="col-md-8 col-lg-8 col-sm-8 form-control"
              name="accountNo"
              component={renderField}
              type="text"
              placeholder="Account No"
              OnformBlur={this.props.OnformBlur}
            />
          </div>
          <div className="form-group">
            <label>Holder Name</label>
            <Field
              className="col-md-8 col-lg-8 col-sm-8 form-control"
              name="holderName"
              component={renderField}
              type="text"
              placeholder="Holder Name"
              OnformBlur={this.props.OnformBlur}
            />
          </div>
          <div className="form-group">
            <label>IFSC Code</label>
            <Field
              className="col-md-8 col-lg-8 col-sm-8 form-control"
              name="IFSC"
              component={renderField}
              type="text"
              placeholder="IFSC Code"
              OnformBlur={this.props.OnformBlur}
            />
          </div>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: "bankdriver", // a unique identifier for this form
  validate // <--- validation function given to redux-form
})(bankInfoForm);
