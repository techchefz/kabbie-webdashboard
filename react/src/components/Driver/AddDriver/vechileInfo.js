import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import PropTypes from "prop-types";
import request from "superagent";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import UserAction from "../../../redux/users/action";
import "../../../styles/common/add_driver.scss";
import VehicleInfoForm from "./vehicleInfoForm";

class VehicleInfo extends Component {
  static propTypes = {
    vehicleobj: PropTypes.object,
    onUpdateVehicleInfo: PropTypes.func,
    uploadDriverFiles: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      st: null,
      stn: null,
      div: null,
      num: null,
      //========================
      mm: null,
      dd: null,
      yyyy: null,
      pwd: null,
      cpwd: null,
      pstate: false,
      //===================
      carDetails: {
        company: null,
        regNo: null,
        regDate: null,
        RC_ownerName: null,
        vehicleNo: null,
        carModel: null,
        type: "Economy"
      },
      vehicleDocuments: {
        insuranceUrl: null,
        rcBookUrl: null,
        vechileBackUrl: null,
        vechileFrontUrl: null
      },
      file1: "",
      file2: "",
      file3: "",
      file4: "",
      imagePreviewUrl: "",
      imageRegPreviewUrl: "",
      imageFrontPreviewUrl: "",
      imageBackPreviewUrl: ""
    };
  }

  selectCar(car) {
    this.setState(prevState => ({
      ...prevState,
      carDetails: {
        ...prevState.carDetails,
        type: car
      }
    }));
    setTimeout(() => {
      this.handleCarSelection();
    }, 1);
  }

  handleCarSelection() {
    this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
  }

  handleChange(inputname, value, label) {
    if (inputname === "month") {
      this.setState({ mm: value });
      const date = `${value}/${this.state.dd}/${this.state.yyyy}`;
      this.state.carDetails["regDate"] = date;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    } else if (inputname === "date") {
      this.setState({ dd: value });
      const date = `${this.state.mm}/${value}/${this.state.yyyy}`;
      this.state.carDetails["regDate"] = date;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    } else if (inputname === "year") {
      this.setState({ yyyy: value });
      // const date = `${this.state.mm}/${this.state.dd}/${value}`;
      // this.handleDate();
      const date = `${this.state.mm}/${this.state.dd}/${value}`;
      this.state.carDetails["regDate"] = date;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
      // this.props.onUpdatePersonalInfo("dob", date);
    } else if (inputname === "state") {
      this.setState({ st: value });
    } else if (inputname === "sno") {
      this.setState({ stn: value });
    } else if (inputname === "div") {
      this.setState({ div: value });
    } else if (inputname === "vehicleno") {
      this.setState({ num: value });
      const vno = this.state.st + this.state.stn + this.state.div + value;
      this.state.carDetails["vehicleNo"] = vno;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    } else {
      this.state.carDetails[inputname] = value;
      this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
    }
  }
  handleVehicleNo(label) {
    const vno =
      this.state.st + this.state.stn + this.state.div + this.state.num;
    this.props.onUpdateVehicleInfo(vno, label, true);
  }

  handleDate() {
    const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
    this.state.carDetails["regDate"] = date;
    this.props.onUpdateVehicleInfo("carDetails", this.state.carDetails);
  }

  _handleImageChange(e, type) {
    e.preventDefault();
    var fileTypes = ["jpg", "jpeg", "png"];
    if (e.target.files[0]) {
      var extension = e.target.files[0].name
          .split(".")
          .pop()
          .toLowerCase(), //file extension from input file
        isSuccess = fileTypes.indexOf(extension) > -1;
      if (isSuccess) {
        let reader = new FileReader();
        let file = e.target.files[0];
        let filezzzz = URL.createObjectURL(e.target.files[0]);
        reader.onloadend = () => {
          if (type === "insurance") {
            this.setState({
              file1: file,
              imagePreviewUrl: reader.result
            }),
              this.handleImageUploadcloudinary(reader.result, type);
          } else if (type === "register") {
            this.setState({
              file2: file,
              imageRegPreviewUrl: reader.result
            }),
              this.handleImageUploadcloudinary(reader.result, type);
          } else if (type === "vehicleFront") {
            this.setState({
              file3: file,
              imageFrontPreviewUrl: reader.result
            }),
              this.handleImageUploadcloudinary(reader.result, type);
          } else if (type === "vehicleBack") {
            this.setState({
              file4: file,
              imageBackPreviewUrl: reader.result
            }),
              this.handleImageUploadcloudinary(reader.result, type);
          }
        };
        reader.readAsDataURL(file);
      } else {
        if (type === "insurance") {
          this.setState({
            file1: "",
            imagePreviewUrl: ""
          });
        } else if (type === "register") {
          this.setState({
            file2: "",
            imageRegPreviewUrl: ""
          });
        } else if (type === "vehicleFront") {
          this.setState({
            file3: "",
            imageFrontPreviewUrl: ""
          });
        } else if (type === "vehicleBack") {
          this.setState({
            file4: "",
            imageBackPreviewUrl: ""
          });
        }

        alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
      }
    } else {
      if (type === "insurance") {
        this.setState({
          file1: "",
          imagePreviewUrl: ""
        });
      } else if (type === "register") {
        this.setState({
          file2: "",
          imageRegPreviewUrl: ""
        });
      } else if (type === "vehicleFront") {
        this.setState({
          file3: "",
          imageFrontPreviewUrl: ""
        });
      } else if (type === "vehicleBack") {
        this.setState({
          file4: "",
          imageBackPreviewUrl: ""
        });
      }
    }
  }

  handleImageUploadcloudinary(file, type) {
    let upload = request
      .post("https://api.cloudinary.com/v1_1/taxiapp1/upload")
      .field("upload_preset", "bkfchx7x")
      .field("file", file);

    upload.end((err, response) => {
      if (err) {
        console.error(err);
      }

      if (response.body.secure_url !== "") {
        if (type === "insurance") {
          (this.state.vehicleDocuments["insuranceUrl"] =
            response.body.secure_url),
            this.props.onUpdateVehicleInfo(
              "vehicleDocuments",
              this.state.vehicleDocuments
            );
        } else if (type === "register") {
          (this.state.vehicleDocuments["rcBookUrl"] = response.body.secure_url),
            this.props.onUpdateVehicleInfo(
              "vehicleDocuments",
              this.state.vehicleDocuments
            );
        } else if (type === "vehicleFront") {
          (this.state.vehicleDocuments["vechileFrontUrl"] =
            response.body.secure_url),
            this.props.onUpdateVehicleInfo(
              "vehicleDocuments",
              this.state.vehicleDocuments
            );
        } else if (type === "vehicleBack") {
          (this.state.vehicleDocuments["vechileBackUrl"] =
            response.body.secure_url),
            this.props.onUpdateVehicleInfo(
              "vehicleDocuments",
              this.state.vehicleDocuments
            );
        }
      }
    });
  }

  render() {
    return (
      <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div className="panel panel-primary adddriverpanel">
          <div className="panel-heading">
            <span>
              {" "}
              <FormattedMessage
                id={"vehicle_info"}
                defaultMessage={"Vehicle Info"}
              />
            </span>
          </div>
          <div
            className="panel-body"
            style={{ height: 700, overflow: "scroll" }}
          >
            <VehicleInfoForm
              OnformBlur={(name, val, label) =>
                this.handleChange(name, val, label)
              }
            />
            <form className="form">
              <div className="col-md-12 col-lg-12">
                <div className="form-group">
                  <label>Car Type:</label>
                  <select
                    className="form-control"
                    type="text"
                    name="Car Type"
                    value={this.state.carDetails.car}
                    onChange={event => this.selectCar(event.target.value)}
                  >
                    <option value="Quickest">Quickest</option>
                    <option value="Sedan">Sedan</option>
                    <option value="Wagon">Wagon</option>
                    <option value="WheelChairOne">Wheel Chair One</option>
                    <option value="WheelChairTwo">Wheel Chair Two</option>
                    <option value="MaxiFIVE">Maxi FIVE</option>
                    <option value="MaxiSIX">Maxi SIX</option>
                    <option value="MaxiSEVEN">Maxi SEVEN</option>
                    <option value="MaxiEIGHT">Maxi EIGHT</option>
                    <option value="MaxiNINE">Maxi NINE</option>
                    <option value="MaxiTEN">Maxi TEN</option>
                    <option value="MaxiELEVEN">Maxi ELEVEN</option>
                  </select>
                </div>
              </div>
            </form>
            <div className="previewComponent">
              <Form>
                <div className="form-group">
                  <label>Insurance Certificate:</label>
                  <input
                    className="form-control"
                    type="file"
                    onChange={e => this._handleImageChange(e, "insurance")}
                  />
                </div>
                <Button style={{ border: "none" }} />
              </Form>
              <div className="imgPreview col-sm-12 form-group">
                {this.state.imagePreviewUrl ? (
                  <img alt="Insurance" src={this.state.imagePreviewUrl} />
                ) : (
                  <div className="previewText">
                    Please select an Image for Preview
                  </div>
                )}
              </div>
            </div>
            <div className="previewComponent">
              <Form>
                <div className="form-group">
                  <label>Registration Certificate:</label>
                  <input
                    className="form-control"
                    type="file"
                    onChange={e => this._handleImageChange(e, "register")}
                  />
                </div>
                <Button style={{ border: "none" }} />
              </Form>
              <div className="imgPreview col-sm-12 form-group">
                {this.state.imageRegPreviewUrl ? (
                  <img alt="Reg" src={this.state.imageRegPreviewUrl} />
                ) : (
                  <div className="previewText">
                    Please select an Image for Preview
                  </div>
                )}
              </div>
            </div>
            <div className="previewComponent">
              <Form>
                <div className="form-group">
                  <label>Vehicle Front</label>
                  <input
                    className="form-control"
                    type="file"
                    onChange={e => this._handleImageChange(e, "vehicleFront")}
                  />
                </div>
                <Button style={{ border: "none" }} />
              </Form>
              <div className="imgPreview col-sm-12 form-group">
                {this.state.imageFrontPreviewUrl ? (
                  <img alt="Reg" src={this.state.imageFrontPreviewUrl} />
                ) : (
                  <div className="previewText">
                    Please select an Image for Preview
                  </div>
                )}
              </div>
            </div>
            <div className="previewComponent">
              <Form>
                <div className="form-group">
                  <label>Vehicle Back</label>
                  <input
                    className="form-control"
                    type="file"
                    onChange={e => this._handleImageChange(e, "vehicleBack")}
                  />
                </div>
                <Button style={{ border: "none" }} />
              </Form>
              <div className="imgPreview col-sm-12 form-group">
                {this.state.imageBackPreviewUrl ? (
                  <img alt="Reg" src={this.state.imageBackPreviewUrl} />
                ) : (
                  <div className="previewText">
                    Please select an Image for Preview
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function bindActions(dispatch) {
  return {
    uploadDriverFiles: image => dispatch(UserAction.uploadDriverFiles(image))
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    bindActions
  )(VehicleInfo)
);
