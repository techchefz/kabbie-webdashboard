import React, { Component } from "react";

import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { FormattedMessage } from "react-intl";
import "../../../styles/common/add_driver.scss";

const validate = values => {
  const errors = {};
  if (!values.fname) {
    errors.fname = "Required";
  }
  if (!values.lname) {
    errors.lname = "Required";
  }
  if (!values.email) {
    errors.email = "Required";
  }
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  if (!values.countryCode) {
    errors.countryCode = "Required";
  }
  if (!values.phoneNo) {
    errors.phoneNo = "Phone number is Required";
  }
  if (isNaN(Number(values.phoneNo))) {
    errors.phoneNo = "Phone number must be in digits";
  }
  if (!values.password) {
    errors.password = "Password is Required";
  }
  if (values.password !== values.confirmpassword) {
    errors.confirmpassword = "Entered passwords doesn't match";
  }
  return errors;
};

const renderField = ({
  input,
  label,
  placeholder,
  type,
  className,
  OnformBlur,
  meta: { touched, error, warning }
}) => (
  <div>
    {label !== "dob" ? (
      <label className="col-md-4 col-lg-4 col-sm-4 formlabel"> {label}</label>
    ) : (
      <span style={{ width: 0 }} />
    )}
    <div>
      <input
        className={className}
        {...input}
        type={type}
        placeholder={placeholder}
        onBlur={e => {
          input.onBlur(e);
          OnformBlur(input.name, input.value);
        }}
      />
      {touched &&
        (error && (
          <span style={{ color: "red", textAlign: "center", display: "block" }}>
            {error}
          </span>
        ))}
    </div>
  </div>
);

class personalInfoForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func,
    OnformBlur: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      isfetched: false
    };
  }
  render() {
    return (
      <form className="form">
        <div className="col-md-12 col-lg-12">
          <div className="row">
            <div class="col-sm-6">
              <div className="form-group">
                <label>First Name</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="fname"
                  component={renderField}
                  type="text"
                  placeholder="First Name"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>Last Name</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="lname"
                  component={renderField}
                  type="text"
                  placeholder="Last Name"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div class="col-sm-6">
              <div className="form-group">
                <label>Email Email</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="email"
                  component={renderField}
                  type="email"
                  placeholder="Email Id"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>Contact No</label>
                <div>
                  <div className="col-sm-3">
                    <Field
                      className="col-md-2 col-lg-2 col-sm-2 form-control"
                      name="countryCode"
                      component={renderField}
                      placeholder="91"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-9">
                    <Field
                      className="col-md-6 col-lg-6 col-sm-6 form-control"
                      name="phoneNo"
                      component={renderField}
                      placeholder="Phone No"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>Password</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="password"
                  component={renderField}
                  type="password"
                  placeholder="Password"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>Confirm Password</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="confirmpassword"
                  component={renderField}
                  type="password"
                  placeholder="Confirm Password"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
          </div>
          <div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>DOB</label>
                <div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="month"
                      label="dob"
                      component={renderField}
                      placeholder="MM"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="date"
                      label="dob"
                      component={renderField}
                      placeholder="DD"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                  <div className="col-sm-4 col-xs-4">
                    <Field
                      className="col-md-4 col-lg-4 col-sm-4 form-control"
                      name="year"
                      label="dob"
                      component={renderField}
                      placeholder="YYYY"
                      OnformBlur={this.props.OnformBlur}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div className="form-group">
                <label>Blood Group</label>
                <Field
                  className="col-md-8 col-lg-8 col-sm-8 form-control"
                  name="bloodGroup"
                  component={renderField}
                  placeholder="Blood group"
                  OnformBlur={this.props.OnformBlur}
                />
              </div>
            </div>
          </div>
          {/* <div className="col-md-6 col-lg-6 col-sm-6 formdiv ">
            <Field
              className="col-md-8 col-lg-8 col-sm-8 formfield"
              name="phoneNo"
              component={renderField}
              type="text"
              label={
                <FormattedMessage id={"contact"} defaultMessage={"Contact#"} />
              }
              placeholder="Phone No"
              OnformBlur={this.props.OnformBlur}
            />
          </div> */}
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: "personaldriver", // a unique identifier for this form
  validate // <--- validation function given to redux-form
})(personalInfoForm);
