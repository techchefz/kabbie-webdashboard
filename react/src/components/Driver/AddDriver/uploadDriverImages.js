import React, { Component } from "react";
import { connect } from "react-redux";
import { addUrlToNewUser } from "../../../redux/users/action";
import { FormattedMessage } from "react-intl";
import { Table, Button } from "react-bootstrap";

import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import axios from "axios";

import "./styles.css";

class uploadDriverImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileType: null,
      urls: {
        licenceURL: null,
        insuranceURL: null,
        rcBookUrl: null,
        vechilePaperUrl: null,
        vechileFrontUrl: null,
        vechileBackUrl: null
      }
    };
  }

  handleLicence() {
    this.setState({ fileType: "licence" });
  }
  handleInsurance() {
    this.setState({ fileType: "insurance" });
  }
  handleRCBook() {
    this.setState({ fileType: "rcBook" });
  }
  handleVehiclePaper() {
    this.setState({ fileType: "vehiclePaper" });
  }
  handleVehicleFront() {
    this.setState({ fileType: "vehicleFront" });
  }
  handleVehicleBack() {
    this.setState({ fileType: "vehicleBack" });
  }

  render() {
    this.handleDrop = files => {
      // Push all the axios request promise into a single array
      const uploaders = files.map(file => {
        // Initial FormData
        const formData = new FormData();
        formData.append("file", file);
        formData.append("tags", this.state.filetype);
        formData.append("upload_preset", "bhrnsem5"); // Replace the preset name with your own
        formData.append("api_key", "948999689987678"); // Replace API key with your own Cloudinary key
        formData.append("timestamp", (Date.now() / 1000) | 0);
        console.log("going into upload function");

        // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
        return axios
          .post(
            "https://api.cloudinary.com/v1_1/nishantbhadana/image/upload",
            formData,
            {
              headers: { "X-Requested-With": "XMLHttpRequest" }
            }
          )
          .then(response => {
            const data = response.data;
            const fileURL = data.secure_url; // You should store this URL for future references in your app

            switch (this.state.fileType) {
              case "licence":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    licenceURL: fileURL
                  }
                }));
                break;
              case "insurance":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    insuranceURL: fileURL
                  }
                }));
                break;
              case "rcBook":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    rcBookUrl: fileURL
                  }
                }));
                break;
              case "vehiclePaper":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    vechilePaperUrl: fileURL
                  }
                }));
                break;
              case "vehicleFront":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    vechileFrontUrl: fileURL
                  }
                }));
                break;
              case "vehicleBack":
                this.setState(prevState => ({
                  urls: {
                    ...prevState.urls,
                    vechileBackUrl: fileURL
                  }
                }));
                break;

              default:
                alert("Please Try Again ");
            }
          });
      });

      axios.all(uploaders).then(() => {
        this.setState(prevState => ({
          ...prevState,
          fileType: null
        }));
        this.props.updateUrls(this.state.urls);
      });
    };

    return (
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading">
            {" "}
            <FormattedMessage
              id={"uploadDriverImages"}
              defaultMessage="Upload Document Images"
            />
          </div>
          <div className="panel-body">
            <div className="UploadPic">
              <Table className="table2">
                <tbody>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleLicence()}
                      >
                        LICENCE
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.licenceURL === null ? (
                        <p>Click Button to upload Image of Licence</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleInsurance()}
                      >
                        INSURANCE
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.insuranceURL === null ? (
                        <p>Click Button to upload Image of Insurance</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleRCBook()}
                      >
                        RC BOOK
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.rcBookUrl === null ? (
                        <p>Click Button to upload Image of RC Book</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleVehiclePaper()}
                      >
                        VEHICLE PAPER
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.vechilePaperUrl === null ? (
                        <p>Click Button to upload Image of Vehicle Paper</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleVehicleFront()}
                      >
                        VEHICLE FRONT
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.vechileFrontUrl === null ? (
                        <p>Click Button to upload Image of Vehicle Front</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Button
                        className="imagebtm"
                        onClick={() => this.handleVehicleBack()}
                      >
                        VEHICLE BACK
                      </Button>
                    </td>
                    <td>
                      {this.state.urls.vechileBackUrl === null ? (
                        <p>Click Button to upload Image of Vehicle Back</p>
                      ) : (
                        <p className="successMessage">Uploaded Successfully</p>
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <div className="ManageUpload">
              {this.state.fileType !== null ? (
                <div>
                  <Dropzone onDrop={this.handleDrop} multiple accept="image/*">
                    <div className="dropZone">
                      Drop your files or click here to upload
                    </div>
                  </Dropzone>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function bindActions(dispatch) {
  return {
    addUrls: Urls => {
      dispatch(Urls);
    }
  };
}

export default uploadDriverImages;
