import React, { Component } from 'react';
import _ from "lodash";
import moment from "moment";
import { FormattedMessage } from "react-intl";
import { Button } from "react-bootstrap";

import "../../styles/common/DriverList.scss"

export default class QueryDetails extends Component {
    render() {
        return (
            <div>
                <div>
                    <table className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <thead>
                            <tr className="panelTableHead" style={{ backgroundColor: "#bdbdbd", color: "white", border: "5px white solid" }}>
                                <td>
                                    <strong style={{ marginLeft: "45%" }}>  <FormattedMessage

                                        id={"queryDetails"}
                                        defaultMessage={"QUERY DETAILS"}
                                    /> </strong>
                                </td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody className="panelTableTBody">
                            <tr>
                                <td>QUERY ID</td>
                                <td><strong>{_.get(this.props.queryObj, "_id", "NA")}</strong></td>
                            </tr>
                            <tr>
                                <td>NAME</td>
                                <td>{_.get(this.props.queryObj.userId, "fname", "NA")} {_.get(this.props.queryObj.userId, "lname", "NA")}</td>
                            </tr>

                            <tr>
                                <td>CONTACT</td>
                                <td>{_.get(this.props.queryObj.userId, "phoneNo", "NA")}</td>
                            </tr>
                            <tr>
                                <td>EMAIL</td>
                                <td>{_.get(this.props.queryObj.userId, "email", "NA")}</td>
                            </tr>
                            <tr>
                                <td>QUERY TIME</td>
                                <td>{moment(_.get(this.props.queryObj, "submittedOn", "NA")).format("DD MMM YYYY hh:mm a")}</td>
                            </tr>
                            <tr>
                                <td>SUBJECT</td>
                                <td>{_.get(this.props.queryObj, "querySubject", "NA")}</td>
                            </tr>
                            <tr>
                                <td>MESSAGE</td>
                                <td>{_.get(this.props.queryObj, "queryMessage", "NA")}</td>
                            </tr>
                            <tr>
                                <td>STATUS</td>
                                <td>{_.get(this.props.queryObj, "queryStatus", "NA")}</td>
                            </tr>
                            {this.props.queryObj.queryStatus !== "Resolved" ?
                                <tr>
                                    <td>CHANGE STATUS</td>
                                    <td>
                                        {
                                            this.props.queryObj.queryStatus !== "In Progress" ?
                                                <Button style={{ backgroundColor: "#FFC700", color: "#FFF" }} onClick={() => this.props.changeQueryStatus("In Progress")}>IN PROGRESS</Button>
                                                : null
                                        }
                                        <Button style={{ backgroundColor: "#0B6623", color: "#FFF" }} onClick={() => this.props.changeQueryStatus("Resolved")}>RESOLVED</Button>
                                    </td>
                                </tr> : null}
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
}
