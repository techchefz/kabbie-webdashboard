import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import Lightbox from "react-image-lightbox";
import { withRouter } from "react-router-dom";
import {  Button, Tabs, Tab } from "react-bootstrap";
import UserAction from "../../../../redux/userDetails/action";
import { updateUserObject } from "../../../../redux/updateUser/action";
import Rating from "../../../../components/UserRatingComponent";
import "../../../../styles/common/DriverProfile.scss";
import config from "../../../../services/apiConfig";

class EditProfile extends Component {
  static propTypes = {
    userprofiledetails: PropTypes.any,
    loading: PropTypes.bool
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isOpen: false,
      imageUrl: "",
      updatedUser: null
    };
    this.setLoading = this.setLoading.bind(this);
  }
  componentWillMount() {
    this.setState({
      ...this.state,
      updatedUser: this.props.userprofiledetails
    });
  }
  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }
  updateUser() {
    const userObj = this.state.updatedUser;
    this.props.updatedUser(userObj);
    this.props.history.push("/driver-list");
  }
  openLIghtBox(url) {
    this.setState({ isOpen: true, imageUrl: url });
  }
  render() {
    const user = this.props.userprofiledetails;
    return (
      <div>
        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 profilediv">
          <div className="row">
            <div className="col-xs-4 col-sm-2">
              <img
                alt="profile"
                src={this.props.userprofiledetails.profileUrl}
                className="image"
                onClick={() =>
                  this.openLIghtBox(this.props.userprofiledetails.profileUrl)
                }
              />
            </div>
            <div className="col-xs-8 col-sm-4">
              <div className="profdetails">
                <div>
                  <span className="profname}">
                    <input
                      type="text"
                      value={this.state.updatedUser.fname}
                      onChange={event =>
                        this.setState({
                          ...this.state,
                          updatedUser: {
                            ...this.state.updatedUser,
                            fname: event.target.value
                          }
                        })
                      }
                    />
                    <input
                      type="text"
                      value={this.state.updatedUser.lname}
                      onChange={event =>
                        this.setState({
                          ...this.state,
                          updatedUser: {
                            ...this.state.updatedUser,
                            lname: event.target.value
                          }
                        })
                      }
                    />
                  </span>
                  <span className="profrating">
                    <Rating
                      noofstars={5}
                      ratedStar={this.props.userprofiledetails.userRating}
                    />
                  </span>
                </div>
                <div className="namephone">
                  <span className="phone">
                    {" "}
                    <span
                      style={{ color: "#bbb" }}
                      className="glyphicon glyphicon-phone"
                    />
                    <span
                      style={{ fontSize: 12, marginLeft: 5, color: "#bbb" }}
                    >
                      {this.props.userprofiledetails.phoneNo}
                    </span>
                  </span>
                  <span className="email">
                    {" "}
                    <span
                      style={{ marginLeft: 5, fontSize: 12, color: "#bbb" }}
                      className="glyphicon glyphicon-envelope"
                    />
                    <span
                      style={{ fontSize: 12, marginLeft: 5, color: "#bbb" }}
                    >
                      {this.props.userprofiledetails.email}
                    </span>
                  </span>
                </div>
              </div>
            </div>
            <div className="profile-btn-group">
              <Button
                style={{
                  display: "block",
                  float: "right",
                  backgroundColor: "#008000",
                  color: "white",
                  border: "none"
                }}
                bsStyle="primary"
                onClick={() => this.updateUser()}
              >
                {" "}
                <FormattedMessage id={"save"} defaultMessage={"SAVE"} />
              </Button>
              <Button
                style={{
                  display: "block",
                  float: "right",
                  backgroundColor: "#005580",
                  color: "white",
                  border: "none"
                }}
                bsStyle="primary"
                onClick={() =>
                  this.props.history.push(
                    `/driver-profile/${this.state.updatedUser._id}`
                  )
                }
              >
                {" "}
                <FormattedMessage id={"cancel"} defaultMessage={"CANCEL"} />
              </Button>
            </div>
          </div>
        </div>
        <Tabs
          defaultActiveKey={1}
          animation={false}
          id="noanim-tab-example"
          className="col-lg-12 col-md-12 col-sm-12 col-xs-12"
          style={{ float: "left" }}
        >
          <Tab eventKey={1} title="Details">
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 tabdiv">
              <div className="panel panel-primary">
                <div
                  className="panel-body panelTableBody"
                  style={{ padding: 10 }}
                >
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div className="panel panel-primary">
                      <div className="panel-heading">
                        <span>Licence Details</span>
                      </div>
                      <div className="panel-body panelBody driverDetailsPanel">
                        <div className="contentdiv">
                          <span className="title">Licence No:</span>
                          <input
                            type="text"
                            value={
                              this.state.updatedUser.licenceDetails.licenceNo
                            }
                            onChange={event =>
                              this.setState({
                                ...this.state,
                                updatedUser: {
                                  ...this.state.updatedUser,
                                  licenceDetails: {
                                    ...this.state.updatedUser.licenceDetails,
                                    licenceNo: event.target.value
                                  }
                                }
                              })
                            }
                          />
                        </div>
                        <div className="contentdiv">
                          <span className="title">Licence Issue Date:</span>
                          <span>
                            <input
                              type="text"
                              value={
                                this.state.updatedUser.licenceDetails.issueDate
                              }
                              onChange={event =>
                                this.setState({
                                  ...this.state,
                                  updatedUser: {
                                    ...this.state.updatedUser,
                                    licenceDetails: {
                                      ...this.state.updatedUser.licenceDetails,
                                      issueDate: event.target.value
                                    }
                                  }
                                })
                              }
                            />
                          </span>
                        </div>
                        <div className="contentdiv">
                          <span className="title">Licence Expiry Date:</span>
                          <span>
                            <input
                              type="text"
                              value={
                                this.state.updatedUser.licenceDetails.expDate
                              }
                              onChange={event =>
                                this.setState({
                                  ...this.state,
                                  updatedUser: {
                                    ...this.state.updatedUser,
                                    licenceDetails: {
                                      ...this.state.updatedUser.licenceDetails,
                                      expDate: event.target.value
                                    }
                                  }
                                })
                              }
                            />
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-primary">
                      <div className="panel-heading">
                        <span>Bank Details</span>
                      </div>
                      <div className="panel-body panelBody driverDetailsPanel">
                        <div className="contentdiv">
                          <span className="title">Account Number:</span>
                          <span className="tittleValue">
                            <input
                              type="text"
                              value={
                                this.state.updatedUser.bankDetails.accountNo
                              }
                              onChange={event =>
                                this.setState({
                                  ...this.state,
                                  updatedUser: {
                                    ...this.state.updatedUser,
                                    bankDetails: {
                                      ...this.state.updatedUser.bankDetails,
                                      accountNo: event.target.value
                                    }
                                  }
                                })
                              }
                            />
                          </span>
                        </div>
                        <div className="contentdiv">
                          <span className="title">Account Holder Name:</span>
                          <span className="tittleValue">
                            <input
                              type="text"
                              value={
                                this.state.updatedUser.bankDetails.holderName
                              }
                              onChange={event =>
                                this.setState({
                                  ...this.state,
                                  updatedUser: {
                                    ...this.state.updatedUser,
                                    bankDetails: {
                                      ...this.state.updatedUser.bankDetails,
                                      holderName: event.target.value
                                    }
                                  }
                                })
                              }
                            />
                          </span>
                        </div>
                        <div className="contentdiv">
                          <span className="title">IFSC Code:</span>
                          <span className="tittleValue">
                            <input
                              type="text"
                              value={this.state.updatedUser.bankDetails.IFSC}
                              onChange={event =>
                                this.setState({
                                  ...this.state,
                                  updatedUser: {
                                    ...this.state.updatedUser,
                                    bankDetails: {
                                      ...this.state.updatedUser.bankDetails,
                                      IFSC: event.target.value
                                    }
                                  }
                                })
                              }
                            />
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-primary">
                      <div className="panel-heading">
                        <span>Car Details</span>
                      </div>
                      <div className="panel-body panelBody driverDetailsPanel">
                        <div className="col-md-12 col-sm-12 col-lg-12">
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Car Type:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <select
                                className="form-control"
                                type="text"
                                name="Car Type"
                                value={this.state.updatedUser.carDetails.type}
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        type: event.target.value
                                      }
                                    }
                                  })
                                }
                              >
                                <option value="Quickest">Quickest</option>
                                <option value="Sedan">Sedan</option>
                                <option value="Wagon">Wagon</option>
                                <option value="WheelChairOne">
                                  Wheel Chair One
                                </option>
                                <option value="WheelChairTwo">
                                  Wheel Chair Two
                                </option>
                                <option value="MaxiFIVE">Maxi FIVE</option>
                                <option value="MaxiSIX">Maxi SIX</option>
                                <option value="MaxiSEVEN">Maxi SEVEN</option>
                                <option value="MaxiEIGHT">Maxi EIGHT</option>
                                <option value="MaxiNINE">Maxi NINE</option>
                                <option value="MaxiTEN">Maxi TEN</option>
                                <option value="MaxiELEVEN">Maxi ELEVEN</option>
                              </select>
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Car Model:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={
                                  this.state.updatedUser.carDetails.carModel
                                }
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        carModel: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Company:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={
                                  this.state.updatedUser.carDetails.company
                                }
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        company: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Vehicle No:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={
                                  this.state.updatedUser.carDetails.vehicleNo
                                }
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        vehicleNo: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              RC Owner Name:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={
                                  this.state.updatedUser.carDetails.RC_ownerName
                                }
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        RC_ownerName: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Registration Date:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={
                                  this.state.updatedUser.carDetails.regDate
                                }
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        regDate: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                          <div className="contentdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Registration Number:
                            </span>
                            <span className="tittleValue col-md-8 col-sm-8 col-lg-8">
                              <input
                                type="text"
                                value={this.state.updatedUser.carDetails.regNo}
                                onChange={event =>
                                  this.setState({
                                    ...this.state,
                                    updatedUser: {
                                      ...this.state.updatedUser,
                                      carDetails: {
                                        ...this.state.updatedUser.carDetails,
                                        regNo: event.target.value
                                      }
                                    }
                                  })
                                }
                              />
                            </span>
                          </div>
                        </div>
                        <div className="col-md-12 col-sm-12 col-lg-12">
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              insurance file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.insuranceUrl}
                              alt="insurance url"
                              onClick={() =>
                                this.openLIghtBox(user.insuranceUrl)
                              }
                            />
                          </div>
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Vehicle paper file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.vechilePaperUrl}
                              alt="insurance url"
                              onClick={() =>
                                this.openLIghtBox(user.vechilePaperUrl)
                              }
                            />
                          </div>
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              RC Book file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.rcBookUrl}
                              alt="insurance url"
                              onClick={() => this.openLIghtBox(user.rcBookUrl)}
                            />
                          </div>
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Licence file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.licenceUrl}
                              alt="insurance url"
                              onClick={() => this.openLIghtBox(user.licenceUrl)}
                            />
                          </div>
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Vehicle Front file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.vechileFrontUrl}
                              alt="insurance url"
                              onClick={() =>
                                this.openLIghtBox(user.vechileFrontUrl)
                              }
                            />
                          </div>
                          <div className="imgdiv col-md-6 col-sm-6 col-lg-6">
                            <span className="title col-md-4 col-sm-4 col-lg-4">
                              Vehicle Back file:
                            </span>
                            <img
                              className="col-md-8 col-sm-8 col-lg-8"
                              src={user.vechileBackUrl}
                              alt="insurance url"
                              onClick={() =>
                                this.openLIghtBox(user.vechileBackUrl)
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Tab>
        </Tabs>
        {this.state.isOpen && (
          <Lightbox
            mainSrc={this.state.imageUrl}
            onCloseRequest={() => this.setState({ isOpen: false })}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userprofiledetails: state.userprofiledetails.userObject,
    loading: state.tripDetails.tripLoading,
    driverStatus: state.currentUser.status
  };
}
function bindActions(dispatch) {
  return {
    updateActiveStatus: (id, updateType) =>
      dispatch(UserAction.updateActiveState(id, updateType)),
    updatedUser: userObj => dispatch(updateUserObject(userObj))
  };
}
export default withRouter(
  connect(
    mapStateToProps,
    bindActions
  )(EditProfile)
);
