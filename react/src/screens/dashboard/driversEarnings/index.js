import React, { Component } from "react";
import Layout from "../../../components/Layout/layout";
import DriverEarningsManagement from "./DriverEarningsManagement";

export default class DriverEarnings extends Component {
  render() {
    return (
      <Layout>
        <DriverEarningsManagement />
      </Layout>
    );
  }
}
