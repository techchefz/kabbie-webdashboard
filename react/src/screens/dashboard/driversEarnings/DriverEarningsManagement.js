import React, { Component } from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import {
  Pagination,
  Button,
  FormControl,
  InputGroup,
  Modal
} from "react-bootstrap";
import _ from "lodash";
import { Link, withRouter } from "react-router-dom";
import UserAction from "../../../redux/users/action";
import UserProfileAction from "../../../redux/userProfile/action";
import moment from "moment";
import { fetchRevenue } from "../../../redux/driverEarnings/action";

class DriverEarningsManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usersName: "",
      searchTerm: "",
      ridesList: null,
      searchType: "MOBILE",
      openDetails: false,
      selectedRide: null,
      activePage: 1,
      show: false,
      email: "",
      phoneNo: ""
    };
    this.handlePagination = this.handlePagination.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  componentWillMount() {
    this.props.fetchUsers(this.state.activePage, "driver");
  }

  fetchRevenue(userId) {
    this.props.fetchRevenue(userId);
  }
  handleClose() {
    this.setState({ ...this.state, show: false });
  }

  handleShow(item) {
    const name = `${item.fname} ${item.lname}`;
    const email = item.email;
    const phoneNo = item.phoneNo;
    this.setState({
      ...this.state,
      show: true,
      usersName: name,
      email: email,
      phoneNo: phoneNo
    });
  }

  handlePagination(eventkey) {
    this.setState({
      ...this.state,
      activePage: eventkey
    });
    this.props.fetchUsers(eventkey, "driver"); //eslint-disable-line
  }

  timeDiff(time) {
    return moment().diff(time, "days");
  }

  todayRevenue() {
    var total = 0;
    this.props.userRevenueList.map(function(trip) {
      if (moment().diff(trip.bookingTime, "days") === 0) {
        total = total + trip.tripAmt;
      }
    });
    return total;
  }

  weekRevenue() {
    var total = 0;
    _.map(this.props.userRevenueList, trip => {
      if (moment().diff(trip.bookingTime, "weeks") === 0) {
        total = total + trip.tripAmt;
      }
    });
    return total;
  }

  monthRevenue() {
    var total = 0;
    _.map(this.props.userRevenueList, trip => {
      if (moment().diff(trip.bookingTime, "months") === 0) {
        total = total + trip.tripAmt;
      }
    });
    return total;
  }

  yearRevenue() {
    var total = 0;
    _.map(this.props.userRevenueList, trip => {
      if (moment().diff(trip.bookingTime, "years") === 0) {
        total = total + trip.tripAmt;
      }
    });
    return total;
  }

  totalRevenue() {
    var total = 0;
    _.map(this.props.userRevenueList, trip => {
      total = total + trip.tripAmt;
    });
    return total;
  }

  render() {
    return (
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div className="panel panel-primary">
          <div className="panel-heading panelheading">
            <span>
              {" "}
              <FormattedMessage
                id={"drivers_list"}
                defaultMessage={"Drivers List"}
              />
            </span>
            <div
              className="col-sm-4 col-xs-4"
              style={{
                float: "right",
                marginTop: -7,
                marginRight: -10,
                display: "none"
              }}
            >
              <InputGroup className="col-sm-8 col-xs-8 inputgroup">
                <InputGroup.Addon className="inputaddon">
                  >
                  <span
                    className="glyphicon glyphicon-search"
                    style={{ color: "#bbb" }}
                  />
                </InputGroup.Addon>
                <FormControl
                  type="text"
                  className="forminput"
                  placeholder="ie Suraj,Tata Tiago,KA-25-MX-007 etc"
                />
              </InputGroup>
            </div>
          </div>
          <div className="panel-body panelTableBody">
            <div className="table-responsive">
              <table className="col-xs-12 panelTable">
                <thead>
                  <tr className="panelTableHead">
                    <th className="col-md-2">
                      {" "}
                      <FormattedMessage id={"name"} defaultMessage={"Name"} />
                    </th>
                    <th className="col-md-2">
                      {" "}
                      <FormattedMessage
                        id={"vehicle_model"}
                        defaultMessage={"Vehicle Model"}
                      />
                    </th>
                    <th className="col-md-2">
                      {" "}
                      <FormattedMessage
                        id={"reg_no"}
                        defaultMessage={"Reg. No"}
                      />
                    </th>
                    <th className="col-md-1.5">
                      {" "}
                      <FormattedMessage
                        id={"contact_no"}
                        defaultMessage={"Contact No."}
                      />
                    </th>
                    <th className="col-md-1.5">
                      {" "}
                      <FormattedMessage
                        id={"email_id"}
                        defaultMessage={"Email Id"}
                      />
                    </th>
                    <th className="col-md-1" />
                  </tr>
                </thead>

                {this.props.loading == true ? null : (
                  <tbody className="panelTableTBody">
                    {_.get(this.props, "userList.data", "") ? (
                      _.map(
                        this.props.userList.data,
                        (item, index) =>
                          item.userType === "driver" ? (
                            item.isApproved == true ? (
                              <tr
                                key={index}
                                onClick={() => {
                                  this.fetchRevenue(item._id);
                                  this.handleShow(item);
                                }}
                              >
                                <td>
                                  <span style={{ cursor: "pointer" }}>
                                    {item.fname.toUpperCase()}{" "}
                                    {item.lname.toUpperCase()}
                                  </span>
                                </td>
                                <td>
                                  {_.get(item.carDetails, "carModel", "")}
                                </td>
                                <td>{_.get(item.carDetails, "regNo", "")}</td>
                                <td>{item.phoneNo}</td>
                                <td>{item.email}</td>
                              </tr>
                            ) : null
                          ) : null
                      )
                    ) : (
                      <div style={{ padding: 15 }}>
                        <span>No of Drivers are Zero </span>
                      </div>
                    )}
                  </tbody>
                )}
              </table>
            </div>
          </div>
        </div>
        <Pagination
          className="pagination"
          bsSize="medium"
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
          activePage={this.state.activePage}
          onSelect={this.handlePagination}
        />

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title className="text-center">
              <h3>{this.state.usersName}</h3>
              <h3>{this.state.email}</h3>
              <h3>{this.state.phoneNo}</h3>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className="text-center">
            <div className="row">
              <div
                className="col-lg-6 col-md-6 col-sm-12 form-group"
                style={{
                  height: "80px",
                  backgroundColor: "#eee",
                  border: "1px solid black",
                  padding: "10px"
                }}
              >
                <label>Todays Revenue</label>
                <br />
                <h3>$ {this.todayRevenue()}</h3>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-12 form-group"
                style={{
                  height: "80px",
                  backgroundColor: "#eee",
                  border: "1px solid black",
                  padding: "10px"
                }}
              >
                <label>Weeks Revenue</label>
                <br />
                <h3>$ {this.weekRevenue()}</h3>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-12 form-group"
                style={{
                  height: "80px",
                  backgroundColor: "#eee",
                  border: "1px solid black",
                  padding: "10px"
                }}
              >
                <label>Months Revenue</label>
                <br />
                <h3>$ {this.monthRevenue()}</h3>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-12 form-group"
                style={{
                  height: "80px",
                  backgroundColor: "#eee",
                  border: "1px solid black",
                  padding: "10px"
                }}
              >
                <label>Years Revenue</label>
                <br />
                <h3>$ {this.yearRevenue()}</h3>
              </div>
              <div
                className="col-lg-6 col-md-6 col-sm-12 form-group"
                style={{
                  height: "80px",
                  backgroundColor: "#eee",
                  border: "1px solid black",
                  padding: "10px"
                }}
              >
                <label>Total Revenue</label>
                <br />
                <h3>$ {this.totalRevenue()}</h3>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleClose}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    userList: state.users.userList,
    loading: state.users.loading,
    meta: state.users.meta,
    failedUserListApi: state.users.failedUserListApi,
    userRevenueList: state.userRevenue.userRevenue
  };
}
function bindActions(dispatch) {
  return {
    fetchUsers: (pageNo, driver) =>
      dispatch(UserAction.fetchUsers(pageNo, driver)),
    fetchRevenue: userId => dispatch(fetchRevenue(userId)),
    userprofiledetails: user =>
      dispatch(UserProfileAction.userprofiledetails(user))
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    bindActions
  )(DriverEarningsManagement)
);
