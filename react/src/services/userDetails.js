import ApiService from "./apiService";
import config from "./apiConfig";

async function getLastFiveRideDetails(token, id, pageNo = 1) {
  const apiObject = {};
  apiObject.method = "GET";
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/user/${id}?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getUserDetail(token, id) {
  const apiObject = {};
  apiObject.method = "GET";
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/userDetails/${id}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getActiveDriversDetails(token) {
  const apiObject = {};
  apiObject.method = "GET";
  apiObject.authentication = token;
  apiObject.endpoint = "api/admin/activeDriverDetails";
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getActiveCustomersDetails(token) {
  const apiObject = {};
  apiObject.method = "GET";
  apiObject.authentication = token;
  apiObject.endpoint = "api/admin/activeCustomerDetails";
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getApprovePendingUsers(token, userType) {
  const apiObject = {};
  apiObject.method = "GET";
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/approvePendingUsers/?userType=${userType}`; // new
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function approveSelectedPendingUser(token, id) {
  // const apiObject = {};
  // apiObject.method = "PUT";
  // apiObject.authentication = token;
  // apiObject.endpoint = `api/admin/approveUser/?id=${id}`;

  // console.log('=============apiObject=======================');
  // console.log(apiObject);
  // console.log('=============apiObject=======================');
  // const response = await ApiService.callApi(apiObject);
  // return response;
  fetch(` ${config.serverUrl}:${config.port}/api/admin/approveUser/?id=${id}`, {
    method: "PUT",
    headers: {
      authentication: token
    }
  })
    .then(r => r.json())
    .then(response => {
      console.log("=================response===================");
      console.log(response);
      console.log("================response====================");
      return response;
    });
}
async function rejectSelectedPendingUser(token, id) {
  // const apiObject = {};
  // apiObject.method = "PUT";
  // apiObject.authentication = token;
  // apiObject.endpoint = `api/admin/rejectUser/?id=${id}`;
  fetch(` ${config.serverUrl}:${config.port}api/admin/rejectUser/?id=${id}`, {
    method: "PUT",
    headers: {
      authentication: token
    }
  })
    .then(r => r.json())
    .then(response => {
      console.log("====================================");
      console.log(response);
      console.log("====================================");
      return response;
    });
  // const response = await ApiService.callApi(apiObject);
}

async function getRevenueDeatils(token, userId) {
  console.log("====================================");
  console.log("ye get Call hai");
  console.log("====================================");
  // const apiObject = {};
  // apiObject.method = "GET";
  // apiObject.authentication = token;
  // apiObject._id = userId;
  // apiObject.endpoint = `api/admin/getAllRevenue`; // new
  // const response = await ApiService.callApi(apiObject);
  // console.log("=======hcvjbkhfcvjbg=============================");
  // console.log(response);
  // console.log("====================================");
  // return response;
  await fetch(`${config.serverUrl}:${config.port}/api/admin/getAllRevenue`, {
    method: "GET",
    headers: {
      authentication: token,
      _id: userId
    }
  })
    .then(r => r.json())
    .then(response => {
      console.log("===========fgvhjbknjctyfugv=========================");
      console.log(response);
      console.log("====================================");
      return response;
    });
}

export default {
  getLastFiveRideDetails,
  getUserDetail,
  getActiveDriversDetails,
  getActiveCustomersDetails,
  getApprovePendingUsers,
  approveSelectedPendingUser,
  rejectSelectedPendingUser,
  getRevenueDeatils
};
