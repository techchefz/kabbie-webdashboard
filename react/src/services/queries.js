import ApiService from "./apiService";

async function getAllUserQueries(token) {
    const apiObject = {};
    apiObject.method = "GET";
    apiObject.authentication = token;
    apiObject.endpoint = "api/admin/query/getQuery";
    const response = await ApiService.callApi(apiObject);
    return response;
}

async function updateQueryStatus(token, queryObj) {
    const apiObject = {};
    apiObject.method = "POST";
    apiObject.authentication = token;
    apiObject.endpoint = "api/admin/query/getQuery";
    apiObject.body = queryObj;
    const response = await ApiService.callApi(apiObject);
    return response;
}


export default {
    getAllUserQueries,
    updateQueryStatus
}