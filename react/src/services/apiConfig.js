export default {
  // local server
  serverUrl: "http://188.166.68.8", // use Ip address instead of localhost
  // serverUrl: "http://192.168.43.208", // use Ip address instead of localhostq
  port: 5010 // incase of running api-server (node app) in development
  // port: 3066 //incase of running api-server (node app) in production
  //  port: 443 //incase of running api-server (node app) on heroku
};
