import { FETCHING_USERS_EARNINGS_SUCCESS } from "./actionType";

const initialState = {
  userRevenue: []
};

export const userRevenue = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_USERS_EARNINGS_SUCCESS:
      return { ...state, userRevenue: action.payload };

    default:
      return state;
  }
};
