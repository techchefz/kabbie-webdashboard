import {
  FETCHING_USERS_EARNINGS,
  FETCHING_USERS_EARNINGS_SUCCESS,
  FETCHING_USERS_EARNINGS_FAILED
} from "./actionType";
import config from "../../services/apiConfig";

function fetchingUserRevenue() {
  return {
    type: FETCHING_USERS_EARNINGS
  };
}

function fetchingUserRevenueSuccess(payload) {
  return {
    type: FETCHING_USERS_EARNINGS_SUCCESS,
    payload
  };
}

function fetchingUserRevenueFailed(payload) {
  return {
    type: FETCHING_USERS_EARNINGS_FAILED,
    payload
  };
}

export const fetchRevenue = payload => {
  return (dispatch, getState) => {
    dispatch(fetchingUserRevenue());
    const token = getState().auth.user.jwtAccessToken;
    dispatch({ type: FETCHING_USERS_EARNINGS });
    fetch(`${config.serverUrl}:${config.port}/api/admin/getAllRevenue`, {
      method: "GET",
      headers: {
        authentication: token,
        _id: payload
      }
    })
      .then(r => r.json())
      .then(response => {
        if (response.success) {
          dispatch(fetchingUserRevenueSuccess(response.data));
        } else {
          dispatch(fetchingUserRevenueFailed(response));
        }
      })
      .catch(err => {
        dispatch(fetchingUserRevenueFailed(err));
      });
  };
};
