const USER_PROFILE_DETAILS = "USER_PROFILE_DETAILS";

const initialState = {
  userObject: null,
};

export const userprofiledetails = (state = initialState, action) => {
  switch (action.type) {
    case USER_PROFILE_DETAILS:
      return {
        userObject: action.userObject,
      };
    default:
      return state;
  }
};
