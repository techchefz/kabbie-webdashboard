export const FETCH_USER_QUERIES = "FETCH_USER_QUERIES";
export const FETCH_USER_QUERIES_SUCCESS = "FETCH_USER_QUERIES_SUCCESS";
export const FETCH_USER_QUERIES_FAILED = "FETCH_USER_QUERIES_FAILED";

export const UPDATE_USER_QUERIES_STATUS = "UPDATE_USER_QUERIES_STATUS";
export const UPDATE_USER_QUERIES_STATUS_SUCCESS = "UPDATE_USER_QUERIES_STATUS_SUCCESS";
export const UPDATE_USER_QUERIES_STATUS_FAILED = "UPDATE_USER_QUERIES_STATUS_FAILED";

