import {
    FETCH_USER_QUERIES_SUCCESS,
    UPDATE_USER_QUERIES_STATUS_SUCCESS,
} from './actionType';

const initialState = {
    userQueries: null
}

export const userQueries = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_QUERIES_SUCCESS:
            return { ...state, userQueries: action.payload };

        default:
            return state;
    }
};
